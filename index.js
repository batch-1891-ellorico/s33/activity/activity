
// #3 CREATE FETCH REQUEST USING GET TO RETRIEVE TO DO LIST
// #4 USE MAP METHOD TO RETURN TITLE
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	console.log(data.map(toDo => {
		return toDo.title;
	}));
});


// #5 Create fetch request using the GET method
let singlePost = fetch('https://jsonplaceholder.typicode.com/todos/9')
		.then((response) => response.json())
.then((data) => (data))

// #6 Print a message in the console that will provide title and status
console.log(singlePost)

// #7 Create a fetch request using POST method to create to do list
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	}, 
	body: JSON.stringify({
		userId: "",
		id: "",
		title: "",
		completed: ""
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// #8 Create fetch request using the PUT method that will update to do list
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT', 
	headers: {
		'Content-Type': 'application/json'
	}, 
	body: JSON.stringify({
		title: "secondTitle",
		description: "secondDescription",
		status: "secondStatus",
		dateCompleted: "secondDate",
		userId:"secondUserId"
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// #11 UPDATE TO DO LIST ITEM BY CHANGING STATUS TO COMPLETE

let date = today.getFullYear()+ '-' + (today.getMonth()+1) + '-' + today.getDate()


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT', 
	headers: {
		'Content-Type': 'application/json'
	}, 
	body: JSON.stringify({
		status: "complete", 
		dateCompleted: date
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// #12 CREATE FETCH REQUEST TO DELETE AN ITEM
fetch('https://jsonplaceholder.typicode.com/todos/1', {
method: 'DELETE'
})
.then((response) => response.json())
.then((data) => console.log(data))

